/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ivory.quickloadconverter;

import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import javax.swing.JOptionPane;

import aQute.bnd.annotation.component.Component;
import aQute.bnd.annotation.component.Reference;
import org.lorainelab.igb.menu.api.MenuBarEntryProvider;
import org.lorainelab.igb.menu.api.model.MenuBarParentMenu;
import org.lorainelab.igb.menu.api.model.MenuIcon;
import org.lorainelab.igb.menu.api.model.MenuItem;

/**
 *
 * @author ivory
 */
@Component(immediate = true)
public class MenuBarExtension implements MenuBarEntryProvider {

    private String ICONPATH = "preferences.png";
    private AnnotsConverterFrame annotsConverterFrame;

    @Reference
    public void setSnpConverterFrame(AnnotsConverterFrame annotsConverterFrame) {
        this.annotsConverterFrame = annotsConverterFrame;
    }

    @Override
    public Optional<List<MenuItem>> getMenuItems() {
        MenuItem menuItem;
        menuItem = new MenuItem("Quickload Converter", (Void t) -> {
            //JOptionPane.showMessageDialog(null, "Hello IGB World!  This is gonna be the Quickload converter app!");
            annotsConverterFrame.setVisible(true);
            return t;
        });
        menuItem.setWeight(1000000000);
        try (InputStream resourceAsStream = MenuBarExtension.class.getClassLoader().getResourceAsStream(ICONPATH)) {
            menuItem.setMenuIcon(new MenuIcon(resourceAsStream));
        } catch (Exception ex) {
        }
        return Optional.ofNullable(Arrays.asList(menuItem));
    }

    @Override
    public MenuBarParentMenu getMenuExtensionParent() {
        return MenuBarParentMenu.TOOLS;
    }
}
