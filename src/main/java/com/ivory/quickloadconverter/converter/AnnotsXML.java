/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ivory.quickloadconverter.converter;

import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author ivory
 */
public class AnnotsXML {
    Document doc;
    Element currentFileElement;
    Element rootElement;
    
    AnnotsXML () throws ParserConfigurationException{
            // Create the empty XML document
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        doc = dBuilder.newDocument();

        // root element
        rootElement = doc.createElement("files");
        doc.appendChild(rootElement);
    }
    
    void addFile(){
        currentFileElement = doc.createElement("file");
        rootElement.appendChild(currentFileElement);
    }
    
    void addAttribute(String attrName, Object attrValue){
        Attr attr = doc.createAttribute(attrName);
        attr.setValue(attrValue.toString());
        currentFileElement.setAttributeNode(attr);
    }
        
    void writeAnnotsFile(String outFile) throws TransformerConfigurationException, TransformerException{
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        DOMSource source = new DOMSource(doc);
        StreamResult result = new StreamResult(new File(outFile));
        transformer.transform(source, result);
    }
    
}
