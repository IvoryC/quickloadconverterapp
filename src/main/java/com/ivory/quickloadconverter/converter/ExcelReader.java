package com.ivory.quickloadconverter.converter;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import javax.swing.JTextArea;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import java.io.File;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;

/**
 * Created by Ivory May 2018
 */
public class ExcelReader {
    
    public void ReadFile(String inFile, String outFile, Boolean allowCustom, JTextArea textBox) throws IOException, InvalidFormatException, ParserConfigurationException {

        System.out.println("Input file is: " + inFile);
        if (textBox != null) {
            System.out.println("Using text box.");
            textBox.append("Using file: " + inFile + "\n");
        } else {
            System.out.println("No text box.");
        }

        // Creating a Workbook from an Excel file (.xls or .xlsx)
        Workbook workbook = WorkbookFactory.create(new File(inFile));

        // Getting the Sheet at index zero
        Sheet sheet = workbook.getSheetAt(0);

        // Create a DataFormatter to format and get each cell's value as String
        DataFormatter dataFormatter = new DataFormatter();

        // You can obtain a rowIterator and columnIterator and iterate over them
        System.out.println("\n\nIterating over Rows and Columns using Iterator\n");
        Iterator<Row> rowIterator = sheet.rowIterator();

        //Get the header
        Row header = rowIterator.next();
        int numCols = header.getLastCellNum();
        String[] heads = new String[numCols];
        boolean[] validColumn = new boolean[numCols];
        String colName;
        Cell headerCell;
        int nameColIndex = -1;

        textBox.append("\nThe following columns will be used as attributes:\n");
        for (int i = 0; i < numCols; i++) {
            headerCell = header.getCell(i);
            colName = dataFormatter.formatCellValue(headerCell);
            heads[i] = colName;
            validColumn[i] = allowCustom | QuickloadDataFile.expectedFields.contains(heads[i]);
            if (validColumn[i]) {
                textBox.append(heads[i] + "\n");
            }
            if (heads[i].equals("name")){
                nameColIndex = i;
            }
        }
        textBox.append("\n");
        if (nameColIndex == -1){
            textBox.append("\n\nError: Input must have a 'name' column.\n\n");
            textBox.setForeground(java.awt.Color.red);
            System.out.println("Input must have a 'name' column.");
            return;
        }

        // Create the empty XML document
        AnnotsXML annots = new AnnotsXML();
        QuickloadDataFile.clearFailSet();

        //For each row, create a new element in the xml document.
        //Create a QuickloadDataFile object to hold the attributes
        //At the end, add the attributes to the xml element.
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            annots.addFile();

            // Iterate over the columns of the current row
            // print all data to std out - indicate if it is used or not
            // print the used data to the UI console
            // add the used data to the XML doc
            Iterator<Cell> cellIterator = row.cellIterator();
            System.out.println("reading row:" + row.getRowNum());

            //get the name field, use that to create a new QuickloadDataFile instance
            Cell nameCell = row.getCell(nameColIndex);
            String nameValue = dataFormatter.formatCellValue(nameCell);
            if (nameValue==null | nameValue.length()==0){
                textBox.append("\n\nError: Each entry must have a 'name' attribute.\n\n");
                textBox.setForeground(java.awt.Color.red);
                System.out.println("Input must have a 'name' entry.");
                return;
            }
            QuickloadDataFile data = new QuickloadDataFile(nameValue);
            for (int i = 0; i < numCols; i++) {
                colName = heads[i];

                //Cell cell = cellIterator.next();
                Cell cell = row.getCell(i);
                String cellValue = dataFormatter.formatCellValue(cell);

                if (validColumn[i] & cellValue.length() > 0) {
                    //annots.addAttribute(heads[i], cellValue);
                    Boolean isGood = data.validateInput(heads[i], cellValue);
                    if (isGood){
                        System.out.println(colName + ":" + cellValue + "\t-->\tUsed");
                    }else{
                        System.out.println(colName + ":" + cellValue + "\t-->\tNot used: failed validation.");
                    }
                } else if (cellValue.length() == 0) {
                    System.out.println(colName + "\t-->\tEmpty value, not included in xml to preserve defaults.");
                } else {
                    System.out.println(colName + ":" + cellValue + "\t-->\tUnexpected attribute, not used.");
                }
            }
            data.addAttributesToDoc(annots);

            System.out.println();
        }
        
        if (QuickloadDataFile.allFailingAttrs.size() > 0) {
            //Inform the user about columns that did not pass validation
            textBox.append("\n\nThe following columns had one or more values which failed in validation:" + "\n");
            Iterator it = QuickloadDataFile.allFailingAttrs.iterator();
            while (it.hasNext()) {
                String failCol = it.next().toString();
                textBox.append(failCol + "\n");
            }
            textBox.append("\nSee help > show console, to see which individual values failed." + "\n");
            textBox.setForeground(java.awt.Color.orange);
        }
        

        // Closing the workbook
        workbook.close();
        
        // write the content into xml file
        try {
            annots.writeAnnotsFile(outFile);
            textBox.append("\nOutput saved as: " + outFile + "\n");
        } catch (TransformerException ex) {
            Logger.getLogger(ExcelReader.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private static void printCellValue(Cell cell) {
        switch (cell.getCellTypeEnum()) {
            case BOOLEAN:
                System.out.print(cell.getBooleanCellValue());
                break;
            case STRING:
                System.out.print(cell.getRichStringCellValue().getString());
                break;
            case NUMERIC:
                if (DateUtil.isCellDateFormatted(cell)) {
                    System.out.print(cell.getDateCellValue());
                } else {
                    System.out.print(cell.getNumericCellValue());
                }
                break;
            case FORMULA:
                System.out.print(cell.getCellFormula());
                break;
            case BLANK:
                System.out.print("");
                break;
            default:
                System.out.print("");
        }

        System.out.print("\t");
    }
}
