/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ivory.quickloadconverter.converter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * See https://wiki.transvar.org/display/igbman/About+annots.xml
 * 
 * @author ivory
 */
public class QuickloadDataFile {
    private String name, index, title, description, url;
    private String background, foreground, positive_strand_color, negative_strand_color;
    private Integer max_depth, name_size;
    private Boolean connected, show2tracks;
    private String load_hint;//enum?
    private String label_field; //enum?
    private String direction_type; //enum?
    
    /*
    This list of accepted label fields currenlty only contains fields for bed files.
    */
    private HashSet<String> knownLabelFields = new HashSet<>(Arrays.asList("none", "descriptoin", "forward", "id", "name", "score", "seq id", "title", "type"));
    
    /*
    This list is assumed to be a comprehensive list (based on the info on the annots wiki page).
    */
    private HashSet<String> knownDirectionTypes = new HashSet<>(Arrays.asList("arrow", "color", "both", "none"));
            
    /*
    Ideally, all attributes are the expected ones with valid inputs.
    It is possible to use custom attributes, which will be stored in customAttrs.
    Any expected attributes that don't pass validation will be noted in a 
    set (failingAttrs) for this entry, and a set (allFailingAttrs) for this annotes file.
    */
    static HashSet<String> allFailingAttrs = new HashSet<>();
    private HashSet<String> failingAttrs;
    private HashMap<String, String> customAttrs;
    
    public static Set<String> expectedFields = new HashSet<>(Arrays.asList(
            "name", "index", "title", 
            "description", "url", "background", "foreground", 
            "positive_strand_color", "negative_strand_color", "max_depth", 
            "name_size", "connected", "show2tracks", "load_hint", 
            "label_field", "direction_type"));

    public QuickloadDataFile(String name) {
        this.name = name;
        failingAttrs = new HashSet<>();
        customAttrs = new HashMap<>();
        index = null;
        title = null;
        description = null;
        url = null;
        background = null;
        foreground = null;
        positive_strand_color = null;
        negative_strand_color = null;
        max_depth = null;
        name_size = null;
        connected = null;
        show2tracks=null;
        load_hint = null;
        label_field = null;
        direction_type = null;
    }
    
    public static void clearFailSet (){
        allFailingAttrs.clear();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    public String getForeground() {
        return foreground;
    }

    public void setForeground(String foreground) {
        this.foreground = foreground;
    }

    public String getPositive_strand_color() {
        return positive_strand_color;
    }

    public void setPositive_strand_color(String positive_strand_color) {
        this.positive_strand_color = positive_strand_color;
    }

    public String getNegative_strand_color() {
        return negative_strand_color;
    }

    public void setNegative_strand_color(String negative_strand_color) {
        this.negative_strand_color = negative_strand_color;
    }

    public int getMax_depth() {
        return max_depth;
    }

    public void setMax_depth(int max_depth) {
        this.max_depth = max_depth;
    }

    public int getName_size() {
        return name_size;
    }

    public void setName_size(int name_size) {
        this.name_size = name_size;
    }

    public boolean isConnected() {
        return connected;
    }

    public void setConnected(boolean connected) {
        this.connected = connected;
    }

    public boolean isShow2tracks() {
        return show2tracks;
    }

    public void setShow2tracks(boolean show2tracks) {
        this.show2tracks = show2tracks;
    }

    public String getLoad_hint() {
        return load_hint;
    }

    public void setLoad_hint(String load_hint) {
        this.load_hint = load_hint;
    }

    public String getLabel_field() {
        return label_field;
    }

    public void setLabel_field(String label_field) {
        this.label_field = label_field;
    }

    public String getDirection_type() {
        return direction_type;
    }

    public void setDirection_type(String direction_type) {
        this.direction_type = direction_type;
    }
    
    /*
    Use this method to assign attribute values.
    Use the set methods to force an attribute.
    */
    public boolean validateInput(String attrName, String attrVal){
        Integer num;
        boolean valid = true;
        switch (attrName){
            case "": 
                valid=false;
                System.out.println("Empty attribute name.");
                break;
            case "name":
                setName(attrVal);
                break;
            case "index":
                setIndex(attrVal);
                break;
            case "title":
                setTitle(attrVal);
                break;
            case "description":
                setDescription(attrVal);
                break;
            case "url":
                setUrl(attrVal);
                break;
            case "background":
                if (isValidColor(attrVal)){
                    setBackground(attrVal);
                }else{
                    valid = false;
                }
                break;
            case "foreground":
                if (isValidColor(attrVal)){
                    setForeground(attrVal);
                }else{
                    valid = false;
                }
                break;
            case "positive_strand_color":
                if (isValidColor(attrVal)){
                    setPositive_strand_color(attrVal);
                }else{
                    valid = false;
                }
                break;
            case "negative_strand_color":
                if (isValidColor(attrVal)){
                    setNegative_strand_color(attrVal);
                }else{
                    valid = false;
                }
                break; 
            case "max_depth":
                try {
                    num = Integer.parseInt(attrVal);
                } catch (Exception e) {
                    num = -1;
                }
                if (num >= 0){
                    setMax_depth(num);
                }else{
                    valid = false;
                }
                break;
            case "name_size":
                try {
                    num = Integer.parseInt(attrVal);
                } catch (Exception e) {
                    num = -1;
                }
                if (num >= 0){
                    setName_size(num);
                }else{
                    valid = false;
                }
                break;
            case "connected":
                if (attrVal.toLowerCase().equals("false")){
                    setConnected(false);
                }else if (attrVal.toLowerCase().equals("true")){
                    setConnected(true);
                }else{
                    valid = false;
                }
                break;
            case "show2tracks":
                if (attrVal.toLowerCase().equals("false")){
                    setShow2tracks(false);
                }else if (attrVal.toLowerCase().equals("true")){
                    setShow2tracks(true);
                }else{
                    valid = false;
                }
                break;
            case "load_hint":
                if (attrVal.equals("Whole Sequence")){
                    setLoad_hint(attrVal);
                }else{
                    valid = false;
                }
                break;
            case "label_field":
                if (!knownLabelFields.contains(attrVal)){
                    allFailingAttrs.add(attrName);
                }
                //alwasy make the assignment, because I don't have a 
                //comprehensive list of label fields to check against.
                setLabel_field(attrVal); 
                break;
            case "direction_type":
                if (knownDirectionTypes.contains(attrVal)){
                    setDirection_type(attrVal);
                }else{
                    valid = false;
                }
                break;
            default: 
                valid=false;
                customAttrs.put(attrName, attrVal);
                break;     
        }
        if (!valid){
            failingAttrs.add(attrName);
            allFailingAttrs.add(attrName);
        }
        System.out.println("Feild: " + attrName + "; with value:" + attrVal + "; was considered " + (valid ? "valid":"not valid") + ".");
        return valid;
    }
    
    private boolean isValidColor (String str){
        boolean valid = false;
        if (str.length() == 6){ valid = true;}
        //as of 9.0.1 IGB allows for both XXXXXX and #XXXXXX in the color string
        if (str.length() == 7 & str.substring(0).equals("#")){ 
            valid = true;
        }
        return valid;
    }
    
    /*
    In the future, if we add the ability to write to other formats, 
    make an interface for the addAttributes method, and reference that interface.
    */
    public void addAttributesToDoc(AnnotsXML annots) {
        // for all attributes of this object, 
        // if they are not null add them to annots
        annots.addAttribute("name", name);
        if (index != null) {
            annots.addAttribute("index", index);
        }
        if (title != null) {
            annots.addAttribute("title", title);
        }
        if (description != null) {
            annots.addAttribute("description", description);
        }
        if (url != null) {
            annots.addAttribute("url", url);
        }
        if (background != null) {
            annots.addAttribute("background", background);
        }
        if (foreground != null) {
            annots.addAttribute("foreground", foreground);
        }
        if (positive_strand_color != null) {
            annots.addAttribute("positive_strand_color", positive_strand_color);
        }
        if (negative_strand_color != null) {
            annots.addAttribute("negative_strand_color", negative_strand_color);
        }
        if (max_depth != null) {
            annots.addAttribute("max_depth", max_depth);
        }
        if (name_size != null) {
            annots.addAttribute("name_size", name_size);
        }
        if (connected != null) {
            annots.addAttribute("connected", connected);
        }
        if (show2tracks != null) {
            annots.addAttribute("show2tracks", show2tracks);
        }
        if (load_hint != null) {
            annots.addAttribute("load_hint", load_hint);
        }
        if (label_field != null) {
            annots.addAttribute("label_field", label_field);
        }
        if (direction_type != null) {
            annots.addAttribute("direction_type", direction_type);
        }

        // add all custom attributes to annots
        Iterator it = customAttrs.entrySet().iterator();
        while (it.hasNext()) {
            HashMap.Entry pair = (HashMap.Entry) it.next();
            annots.addAttribute(pair.getKey().toString(), pair.getValue().toString());
        }
    }

}
