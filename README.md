# Quickload Converter

Installing the Quickload Converter adds a new menu item to the IGB **Tools** menu.

A [Quickload site](http://wiki.transvar.org/display/igbman/Sharing+data+using+QuickLoad+sites) is an excellent way to organize and share data to view in IGB.  Every quickload side includes a file called 'annots.xml' which encodes information about which files should be displayed and how they should appear.  

The IGB users guide includes [a list of attributes](http://wiki.transvar.org/display/igbman/About+annots.xml) that IGB will recognize in an annots.xml file.

## Start with a Template:                      
1. Open the Quickload Converter window (Tools > Quickload Converter)                                           
2. Click "Get Template"                      
3. Select a convenient folder in your file system to save the template file.

This will give you an excel file that you can use to enter your own data.
The first sheet is a template, which includes example entries that you can replace with your own values.
The second sheet has tips and instructions, and the third sheet is a list of recognized attributes and their descriptions.

After you've entered your data into an excel file (such as the provided template), use the quickload converter to put the data into an annots.xml file that you can use for your quicklaod site.

## To run the converter:                      
1. Open the Quickload Converter window (Tools > Quickload Converter)                      
2. Select this excel file as the input file.                      
3. Select a directory where you would like to save the resulting annots.xml file.                      
4. Click "Convert"                      


### Credits

This app was built by Ivory Blakley with help from Deepti Joshi and Sneha Watharkar.

